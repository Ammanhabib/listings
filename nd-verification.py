"""This script crawls data from market downloads file in downloads of PSX"""
import requests
from datetime import date
import datetime
import csv
import boto3

OMTS_URL = 'https://dps.psx.com.pk/download/omts/{}.csv'
NDTRE_URL = 'https://dps.psx.com.pk/download/nd_threshold/{}.pdf'
NDVER_URL = 'https://dps.psx.com.pk/nd-verification'
BUCKET_NAME = 'test-bucket-0645'
RESULT_FILE = '{}-{}-{}.csv'


def get_request(url):
    print("getting requests")
    r = requests.get(url, allow_redirects=True)
    if r.status_code is not 200:
        print("status code is not 200 no file found")
        return r, False

    return r, True


def get_file(d2):

    url = OMTS_URL.format(d2)
    r, status = get_request(url)
    if not status:
        return False
    open('mts.csv', 'wb').write(r.content)
    return True


def save_file(d2):

    print("Saving file to the bucket if already exists updates")
    s3 = boto3.resource('s3')
    year, month, day = d2.split('-')

    my_bucket = s3.Bucket(BUCKET_NAME)
    #updating file(deleting and inserting new one)
    for file in my_bucket.objects.all():
        print(file.key)
        if file.key == RESULT_FILE.format(str(year), str(month), str(day)):
            s3 = boto3.resource('s3')
            s3.Object(BUCKET_NAME, RESULT_FILE.format(str(year), str(month), str(day))).delete()
            s3.meta.client.upload_file(RESULT_FILE.format(str(year), str(month), str(day)),
                                       BUCKET_NAME, RESULT_FILE.format(str(year), str(month), day))
            return
    s3.meta.client.upload_file(RESULT_FILE.format(str(year), str(month), str(day)),
                               BUCKET_NAME, RESULT_FILE.format(str(year), str(month), str(day)))


def check(d2):

    _, status = get_request(OMTS_URL.format(d2))
    if not status:
        return False

    r = requests.head(NDTRE_URL.format(d2))
    if not r.headers:
        return False

    return True


def format_rows(cl_, mem_, d2):
    result_row = []
    result_row.append(["Date", "SETTLEMENT DATE", "MEMBER CODE", "SYMBOL CODE",
                       "COMPANY", "TURNOVER", "RATE", "VALUES", "Type"])

    for m in mem_:
        if 'SETTLEMENT DATE ' in m:
            continue
        result_row.append([m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], "Member to Member"])

    for c in cl_:
        if 'SETTLEMENT DATE ' in c:
            continue
        mem_code = ("(+)"+(c[2])+"(-)"+(c[2]))
        result_row.append([c[0], c[1], mem_code, c[3], c[4], c[5], c[6], c[7], "Client to Client"])

    year, month, day = d2.split('-')

    with open(RESULT_FILE.format(str(year), str(month), str(day)), 'a') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(result_row)


def compare_symbols(d2):
    with open('mts.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        member = False
        client = False
        cl_ = []
        mem_ = []

        for row in readCSV:
            print(row)
            if "MEMBER " in row or "MEMBER" in row or " MEMBER " in row:
                member = True
                client = False
                continue
            elif "CLIENT TO " in row or "CLIENT TO" in row or " CLIENT TO " in row:
                client = True
                member = False
                continue
            if member:
                if len(row) < 1 or not row:
                    continue
                else:
                    mem_.append(row)
            if client:
                if len(row) < 1 or not row:
                    continue
                else:
                    cl_.append(row)

        format_rows(cl_, mem_, d2)


def run():
    today = date.today()
    d2 = today.strftime("%Y-%m-%d")

    if not check(d2):
        return

    r, status = get_request(NDVER_URL)
    if not status:
        return
    if not get_file(d2):
        print("Off market transaction file for the day not found")
        return
    compare_symbols(d2)
    save_file(d2)


if __name__ == '__main__':
    run()
