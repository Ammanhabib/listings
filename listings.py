"""This script crawls data from listings page in PSX."""
import requests
from bs4 import BeautifulSoup
import copy
import csv
import boto3
from datetime import date

BUCKET_NAME = 'test-bucket-0645'
FILE_NAME = 'listings-{}.csv'
OMTS_URL = 'https://dps.psx.com.pk/download/omts/{}.csv'
NDTRE_URL = 'https://dps.psx.com.pk/download/nd_threshold/{}.pdf'
MKT_SUMM = 'https://dps.psx.com.pk/download/mkt_summary/{}.Z'
LISTING_URL = 'https://dps.psx.com.pk/listings'


def get_request(url):
    r = requests.get(url)
    if r.status_code is not 200:
        print("Status code is not 200 no file found.")
        return r, False
    return r, True


def check(d2):
    r, status = get_request(MKT_SUMM.format(d2))
    if not status:
        return False

    s3 = boto3.resource('s3')
    my_bucket = s3.Bucket(BUCKET_NAME)
    for file in my_bucket.objects.all():
        if file.key == FILE_NAME.format(d2):
            print("File already exists for today in the bucket.")
            return False
    return True


def save_file(d2):
    print("Saving file to the bucket")
    s3 = boto3.resource('s3')
    # prams file,bucket,key
    s3.meta.client.upload_file(FILE_NAME.format(d2), BUCKET_NAME, FILE_NAME.format(d2))


def get_dc_table(soup, d2):

    nc_table = soup.find("table", id="dcTable")
    all_trs = nc_table.find("tbody", {"class": "tbl__body"})
    row = []
    all_rows = []
    index = ''
    for tr in all_trs:
        all_tds = tr.findAll("td")
        for td in all_tds:
            div = td.findAll('div', class_='tag')
            if div:
                for d in div:
                    index += d.text+' | '
                row.append(index)
            else:
                row.append(td.text)
        row.append("Defaulter Counter")
        all_rows.append(copy.copy(row))
        row.clear()
        index = ''
    with open(FILE_NAME.format(d2), 'a') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(all_rows)


def get_nc_table(soup, d2):
    nc_table = soup.find("table", id="ncTable")
    all_trs = nc_table.find("tbody", {"class": "tbl__body"})
    row = []
    all_rows = []

    all_rows .append(['Symbol', 'Name', 'Sector', 'Defaulting Clause', 'Clearing Type', 'Shares',
                       'Free Float', 'Listed In', 'Company Type'])
    index = ''
    for tr in all_trs:
        all_tds = tr.findAll("td")
        for td in all_tds:
            div = td.findAll('div', class_='tag')
            if div:
                for d in div:
                    index += d.text+' | '
                row.append(index)
            else:
                if td.text == 'NC':
                    row.append(None)

                row.append(td.text)
        row.append("Normal Counter")
        all_rows.append(copy.copy(row))
        row.clear()
        index = ''

    with open(FILE_NAME.format(d2), 'a') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(all_rows)


def run():
    today = date.today()
    d2 = today.strftime("%Y-%m-%d")
    if not check(d2):
        return
    r, status = get_request(LISTING_URL)
    if not status:
        return
    soup = BeautifulSoup(r.text)
    get_nc_table(soup, d2)
    get_dc_table(soup, d2)
    save_file(d2)


if __name__ == '__main__':
    run()
