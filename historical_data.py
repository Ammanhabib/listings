"This scriot crawls data from market_summary from the start"
"""This script iterates through date and scrapes fetches data from off market transaction summary"""
import requests
from datetime import timedelta, datetime
import csv
import copy

OMTS_URL = 'https://dps.psx.com.pk/download/omts/{}.csv'
NDTRE_URL = 'https://dps.psx.com.pk/download/nd_threshold/{}.pdf'
NDVER_URL = 'https://dps.psx.com.pk/nd-verification'
BUCKET_NAME = 'test-bucket-0645'
RESULT_FILE = '{}-{}-{}.csv'


def get_index(result_row):
    print("getting index of the member code field")
    for row in result_row:
        for cell in row:
            if 'MEMBER CODE' in cell:
                return row.index(cell)


def get_historical_data(d2):
    start_date = '2010-09-07'
    stop_date = d2
    start = datetime.strptime(start_date, "%Y-%m-%d")
    stop = datetime.strptime(stop_date, "%Y-%m-%d")
    while start <= stop:
        date_str = datetime.strftime(start, '%Y-%m-%d')
        r, status = get_request(OMTS_URL.format(date_str))
        if r.status_code == 200:
            if not get_file(date_str):
                continue
            compare_symbols(date_str)

        start = start + timedelta(days=1)


def get_request(url):
    print("getting requests")
    r = requests.get(url, allow_redirects=True)
    if r.status_code is not 200:
        print("status code is not 200 no file found")
        return r, False

    return r, True


def get_file(d2):

    url = OMTS_URL.format(d2)
    r, status = get_request(url)
    if not status:
        return False
    open('mts.csv', 'wb').write(r.content)
    return True


def format_rows(cl_, mem_, d2):
    result_row = []
    row = []
    if len(mem_) < 1 and len(cl_) < 1:
        return
    for m in mem_[0]:
        row.append(m)
    row.append('Type')
    result_row.append(copy.copy(row))
    row.clear()

    for m in mem_:
        if 'MEMBER CODE' in m:
            continue
        for cell in m:
            row.append(cell)
        row.append('Member to member')
        result_row.append(copy.copy(row))
        row.clear()

    for c in cl_:
        if 'MEMBER CODE' in c:
            continue
        index = get_index(result_row)
        c[index] = ("(+)" + (c[index]) + "(-)" + (c[index]))
        for cell in c:
            row.append(cell)
        row.append("Client to client")
        result_row.append(copy.copy(row))
        row.clear()

    year, month, day = d2.split('-')

    with open('nd_verification/'+RESULT_FILE.format(str(year), str(month), str(day)), 'w') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(result_row)


def compare_symbols(d2):
    with open('mts.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        member = False
        client = False
        cl_ = []
        mem_ = []

        for row in readCSV:
            print(row)
            if "MEMBER " in row or "MEMBER" in row or " MEMBER " in row:
                member = True
                client = False
                continue
            elif "CLIENT TO " in row or "CLIENT TO" in row or " CLIENT TO " in row:
                client = True
                member = False
                continue
            if member:
                if len(row) < 1 or not row:
                    continue
                else:
                    mem_.append(row)
            if client:
                if len(row) < 1 or not row:
                    continue
                else:
                    cl_.append(row)

        format_rows(cl_, mem_, d2)


def run():
    today = datetime.today()
    d2 = today.strftime("%Y-%m-%d")
    get_historical_data(d2)


if __name__ == '__main__':
    run()
